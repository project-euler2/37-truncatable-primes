import time
import math
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(2,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def truncatable_prime(limit):
    primes = Prime_Number_In_Range(limit)
    truncatable_primes = []
    for prime in primes:
        truncable = True
        for i in range(1,len(str(prime))):
            if is_prime(int(str(prime)[i:])) == False or is_prime(int(str(prime)[:-i])) == False:
                truncable = False
        if truncable == True:
            truncatable_primes.append(prime)
    return sum(truncatable_primes[4:]) #We start at 4 just to remove 2,3,4,7 form the list

print(truncatable_prime(1000000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )